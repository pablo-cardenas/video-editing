"""Main module."""
from shutil import copyfile
import math
import os

from scipy.io import wavfile
from scipy import ndimage
import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description='Create bash script')
parser.add_argument('--input-audio')
parser.add_argument('--input-frames')
parser.add_argument('--output-audio')
parser.add_argument('--output-frames')

args = parser.parse_args()

AUDIO_RATE, audio_data = wavfile.read(args.input_audio)
AUDIO_COUNT = audio_data.shape[0]

VIDEO_RATE = 29.97
SAMPLES_PER_FRAME = int(AUDIO_RATE / VIDEO_RATE)

DB = []
x = np.arange(AUDIO_COUNT / SAMPLES_PER_FRAME)
chunks = audio_data[:(AUDIO_COUNT//SAMPLES_PER_FRAME)*SAMPLES_PER_FRAME].reshape(-1,SAMPLES_PER_FRAME)
rms = np.mean((chunks / 2 ** 15) ** 2, axis=1)
dB = 10 * np.log10(rms)
print(dB.shape)

hasLoudAudio = dB >= np.arange(-60, -20)[:, np.newaxis]

dilation = ndimage.morphology.binary_dilation(
    ndimage.morphology.binary_erosion(
        hasLoudAudio, np.tile(True, (1, 3))
    ), np.tile(True, (1, 8))
)
hasLoudAudio = np.logical_or(hasLoudAudio, dilation)

y = np.count_nonzero(hasLoudAudio, axis=1) / math.ceil(
    AUDIO_COUNT / SAMPLES_PER_FRAME
)

#plt.subplot(211)
#plt.plot(np.arange(-60, -20), np.gradient(y))
#plt.subplot(212)
#plt.plot(np.arange(-60, -20), y)
#plt.show()

#threshold = int(input("Choose threshold: "))
threshold = -35
nonzeroLoudAudio = np.nonzero(hasLoudAudio[threshold + 60])[0][:-1]
loudAudio = audio_data[
    (nonzeroLoudAudio[:, np.newaxis] * SAMPLES_PER_FRAME + np.arange(SAMPLES_PER_FRAME))
    .flatten()
    .astype(int)
]

wavfile.write(args.output_audio, AUDIO_RATE, loudAudio)

for outputFrame, inputFrame in enumerate(nonzeroLoudAudio):
    src = f"{args.input_frames}/{int(inputFrame)+1:06d}.jpg"
    dst = f"{args.output_frames}/{outputFrame+1:06d}.jpg"
    os.symlink(src, dst)
