SHELL=/bin/bash
OUTDIR = out
TMPDIR = tmp
VIDEODIR = ~/Videos/youtube/
NOISE = 25

VIDEO = measure_theory

#SOURCES := $(sort \
#		  $(wildcard $(VIDEODIR)/${VIDEO}/*.mp4) \
#		  $(wildcard $(VIDEODIR)/${VIDEO}/*.webm) \
#		  $(wildcard $(VIDEODIR)/${VIDEO}/*.mkv))
SOURCES = $(VIDEODIR)/06.mp4


OUTPUTS = out/01.mp4 \
	  out/02.mp4 \
	  out/03.mp4 \
	  out/04.mp4 \
	  out/05.mp4 \
	  out/06.mp4 \
	  out/07.mp4 \
	  out/08.mp4 \
	  out/10.mp4 \
	  out/11.mp4 \
	  out/12.mp4 \
	  out/13.mp4 \
	  out/14.mp4 \
	  out/15.mp4 \
	  out/16.mp4 \
	  out/17.mp4 \
	  out/18.mp4 \
	  out/19.mp4 \
	  out/20.mp4

#OUTPUTS := $(patsubst ${VIDEODIR}/%,${OUTDIR}/%, ${SOURCES})

all: ${OUTPUTS}

$(TMPDIR)/%/audio.wav: ${VIDEODIR}/%.mp4
	mkdir -p tmp/audio
	ffmpeg -i $< -ac 1 tmp/$*/audio.wav -hide_banner

$(TMPDIR)/%/frames/done: ${VIDEODIR}/%.mp4
	mkdir -p tmp/$*/frames
	ffmpeg -i $< -qscale:v 1 tmp/$*/frames/%06d.jpg -hide_banner
	touch $(TMPDIR)/$*/frames/done

$(OUTDIR)/%.mp4 : $(TMPDIR)/%/frames/done $(TMPDIR)/%/audio.wav
	mkdir -p tmp/$*/{newframes,newaudio}
	python cutter.py --input-audio tmp/$*/audio.wav --input-frames ../frames --output-audio tmp/$*/newaudio.wav --output-frames tmp/$*/newframes || true
	ffmpeg -hide_banner -r:v 29.97 -i tmp/$*/newframes/%06d.jpg -i tmp/$*/newaudio.wav -strict -2 $@



#${TMPDIR}/%/labels/${NOISE}.txt: ${VIDEODIR}/%
#	mkdir -p ${TMPDIR}/$*/{silence_detect,silence_vector,labels}
#	ffmpeg -i $< -af "silencedetect=noise=-${NOISE}dB:d=0.1,ametadata=mode=print:file=${TMPDIR}/$*/silence_detect/${NOISE}.txt" -f null - 2> /dev/null
#
#	sed -e '/^frame/d' \
#	    -e '/duration/d' \
#	    -e 's/^.*=//' \
#	    ${TMPDIR}/$*/silence_detect/${NOISE}.txt | tail -n +2 > ${TMPDIR}/$*/silence_vector/${NOISE}.txt
#
#	python labels.py --silence-vector ${TMPDIR}/$*/silence_vector/${NOISE}.txt --output ${TMPDIR}/$*/labels/${NOISE}.txt
#
#${TMPDIR}/%/split.sh: ${TMPDIR}/%/labels/${NOISE}.txt
#	python split.py --labels $< --script ${TMPDIR}/$*/split.sh
#
#${OUTDIR}/%: ${TMPDIR}/%/split.sh
#	mkdir -p ${TMPDIR}/$*/parts
#	sh $< ${VIDEODIR}/$* ${TMPDIR}/$*/parts
#	mkdir -p $(dir ${OUTDIR}/$*)
#	ffmpeg -y -f concat -safe 0 \
#	    -i <(find . -wholename './${TMPDIR}/$*/parts/*.mp4' -printf "file '${PWD}/%p'\n" | sort) \
#	    -c:v libx264 -crf 30 -maxrate 500K -bufsize 1M \
#            -c:a aac -b:a 32k -ac 1 -ar 44100 \
#	    $(addsuffix .mkv, $(basename $@))
#
#.PRECIOUS: %.txt %.sh
