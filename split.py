'''
Create script for splitting
'''

import numpy as np
from argparse import ArgumentParser

parser = ArgumentParser(description='Create bash script')
parser.add_argument('--script', dest='script', help='bash script to generate video parts')
parser.add_argument('--labels', dest='labels')
args = parser.parse_args()

labels = np.loadtxt(args.labels)
num_intervals = len(labels) - 1

with open(args.script, "w") as f:
    for i in range(num_intervals):
        start = labels[i, 0]
        end = labels[i, 1]
        duration = end - start
        print(f'ffmpeg -y -ss {start:07.03f} -i $1 -to {duration:07.03f} ' +
              f'-c:v libx264 -crf 30 -maxrate 500K -bufsize 1M ' +
              f'-c:a aac -b:a 32k -ac 1 -ar 44100' +
              f"  $2/sound_{i:05d}.mp4\n", file=f)

        continue

        start = labels[i, 1]
        end = labels[i+1, 0]
        rate = 0.25
        print(f'ffmpeg -y -ss {start:07.03f} -to {end:07.03f} -i $1 \\', file=f)
        print(f'  -filter_complex " \\', file=f)
        print(f"    [0:v] setpts={rate:5.03f}*(PTS-STARTPTS) [v1{i:05d}];\\", file=f)
        print(f"    [0:a] asetpts={1:5.03f}*(PTS-STARTPTS),atempo={1/rate} [a1{i:05d}]\\", file=f)
        print(f'  " \\', file=f)
        print(f"  -map [v1{i:05d}] -map [a1{i:05d}]\\", file=f)
        print(f"  $2/silence_{i:05d}.mp4\n", file=f)
