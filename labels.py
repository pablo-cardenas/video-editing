import numpy as np
import argparse

parser = argparse.ArgumentParser(description='Create bash script')
parser.add_argument('--silence-vector', dest='silence_vector',
                    help='file with start and end silence time')
parser.add_argument('--output', dest='output')

args = parser.parse_args()

silence_vector = np.loadtxt(args.silence_vector)
labels = np.c_[silence_vector[::2], silence_vector[1::2]]
diff = labels[:, 1] - labels[:, 0]
print(f"time = {np.sum(labels[:, 1] - labels[:, 0])}")
print(f"intervals = {len(labels)}")

# dilation
labels[diff >= 0.1, 0] -= 0.05
labels[diff >= 0.1, 1] += 0.05
labels[diff <= 0.1, 0] -= 0.05  # np.maximum(0.1 - diff[diff <= 0.1]/2, 0)
labels[diff <= 0.1, 1] += 0.05  # np.maximum(0.1 - diff[diff <= 0.1]/2, 0)

# join intersecting
union = []
for begin, end in labels:
    if union and union[-1][1] >= begin:
        union[-1][1] = max(union[-1][1], end)
    else:
        union.append([begin, end])

union = np.array(union)
np.savetxt(f"{args.output}", union, fmt="%.3f", delimiter="\t")
